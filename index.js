import express, { json, urlencoded } from "express";
import { HttpStatus, port } from "./src/config/config.js";
import connectDb from "./src/connectDb/connectDb.js";
import errorHandler from "./src/helper/errorHandler.js";
import { firstRouter } from "./src/router/firstRouter.js";
import { foodRouter } from "./src/router/foodRoute.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import studentRouter from "./src/router/studentRoute.js";
import fileRouter from "./src/router/fileRouter.js";
import bcrypt from "bcrypt";

let expressApp = express();
expressApp.use(json()); //it is used to get req.body put this cod at top so that all route can use req.body
expressApp.use(urlencoded({ extended: true }));
expressApp.use(express.static("./public"));

expressApp.use("/first", firstRouter);
expressApp.use("/food", foodRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/products", productRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/uploads", fileRouter);

expressApp.use(errorHandler);

expressApp.use((err, req, res, next) => {
  let statusCode = err.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
  let message = err?.message || "Internal server error";
  res.status(statusCode).json({
    success: false,
    message,
  });
});



connectDb();

expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});
