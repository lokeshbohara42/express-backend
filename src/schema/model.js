import { model } from "mongoose";
import productSchema from "./productSchema.js";
import reviewSchema from "./reviewSchema.js";
import studentSchema from "./studentSchema.js";

export let Student = model("Student", studentSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);


