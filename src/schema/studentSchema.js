import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    type: String,
  },
  roll: {
    type: Number,
  },
  address: {
    type: String,
  },
},
{
  timestamps: true,
});
export default studentSchema;
