import { Schema } from "mongoose";

let reviewSchema = Schema(
  {
    productRating: {
      type: Number,
      trim: true,
      required: [true, " productRating field is required"],
    },
    comment: {
      type: String,
      trim: true,
      required: [true, "comment field is required"],
    },
    productId: {
      type: Schema.ObjectId,
      ref: "Product",
      trim: true,
      required: [true, "productId field is required"],
    },
  },
  {
    timestamps: true, //it gives create  and updated
  }
);
export default reviewSchema;


