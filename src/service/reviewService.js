import { Review } from "../schema/model.js";


export const createReviewServices = async ({ body }) =>
  await Review.create(body);
export const readReviewDetailsServices = async ({ id }) =>
  await Review.findById(id);
export const readAllReviewDetailsServices = async ({
  find,
  select,
  limit,
  skip,
  sort,
}) => await Review.find({}).select(select).sort(sort).limit(limit).skip(skip);
export const updateReviewDetailsServices = async ({ id, body }) =>
  await Review.findByIdAndUpdate(id, body, {
    new: true,
  });
export const deleteReviewServices = async ({ id }) =>
  await Review.findByIdAndDelete(id);

// if possible don't define variable in the function
