import expressAsyncHandler from "express-async-handler";
import successResponse from "../helper/successResponse.js";
import { HttpStatus } from "../config/config.js";



export let createFile = expressAsyncHandler(async (req, res, next) => {
let result= `localhost:8000/${req.file.filename}`
  successResponse({
    res,
    status: HttpStatus.CREATED,
    result,
  });
});

