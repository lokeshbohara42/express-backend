import { Router } from "express";

import {
  createStudent,
  deleteStudentsDetails,
  readAllStudents,
  readStudentsDetails,
  updateStudentsDetails,
} from "../controller/studentController.js";
import validation from "../middleware/validation.js";
import studentValidation from "../validations/studentValidation.js";

const studentRouter = Router();

studentRouter
  .route("/")
  .post(validation(studentValidation), createStudent)
  .get(readAllStudents);

studentRouter
  .route("/:id")
  .get(readStudentsDetails)
  .patch(validation(studentValidation), updateStudentsDetails)
  .delete(deleteStudentsDetails);

export default studentRouter;
