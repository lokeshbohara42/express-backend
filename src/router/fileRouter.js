import { Router } from "express";
import upload from "../middleware/uploadFiles.js";

const fileRouter = Router();

fileRouter.route("/single").post(upload.single("image"), (req, res) => {
  console.log(req.file);
  console.log(req.body);
  res.json("hello");
});

fileRouter.route("/multiple").post(upload.array("images", 5), (req, res) => {
  console.log(req.files);
  res.json("multiple file");
});

export default fileRouter;
