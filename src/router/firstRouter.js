import { Router } from "express";

export const firstRouter = Router();

//localhost:8000 methode get => i am get
//localhost:8000 methode post => i am post
//localhost:8000 methode patch => i am patch
//localhost:8000 methode delete => i am delete

//Backend/Postman ma hamle get rw delete ma body bata data phatauna milxa--- tara FrontEnd/React ma mildaina.

firstRouter
  .route("/")
  .post((req, res) => {
    console.log(res.body);
    res.json("i am post");
  })
  .get((req, res) => {
    res.json("i am get");
  })
  .patch((req, res) => {
    res.json("i am patch");
  })
  .delete((req, res) => {
    res.json("i am delete");
  });
