export let bookMiddleware = (req, res, next) => {
  console.log("This is book middleware.");
  next();
};

export let bookMiddleware2 = (req, res, next) => {
  console.log("This is book middleware2.");
  next();
};



export let bookMiddleware3 = (role) => 
(req, res, next) => {
    let roleArray = ["admin", "superAdmin"];

    let isInclude = roleArray.includes(role);
    if (isInclude) next();
    else {
      let error = new Error("User is unAuthorized.");
      throw error;
    }

    console.log("This is book middleware3.");
  }

