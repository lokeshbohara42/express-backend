import joi from "joi";

const studentValidation = joi.object().keys({
  name: joi.string().required(),
  roll: joi.number().required(),
  address: joi.string().required(),
});
export default studentValidation;
